package gri;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

public class tab extends UIComponentBase {
	
	private boolean isInitialActiveItem = false;
	
	public void setInitialActiveItem(boolean isInitialActiveItem) {
		this.isInitialActiveItem = isInitialActiveItem;
	}

	public void encodeBegin(FacesContext context) throws java.io.IOException {
		ResponseWriter writer = context.getResponseWriter();
		writer.startElement("div", this);
		writer.writeAttribute("role", "tabpanel", null);
		if (!isInitialActiveItem) {
			writer.writeAttribute("class", "tab-pane", null);
		}
		else {
			writer.writeAttribute("class", "tab-pane active", null);
		}
		writer.writeAttribute("id", TabHelper.getIdTab(this), null);
	}
	
	public void encodeEnd(FacesContext context) throws java.io.IOException {
		ResponseWriter writer = context.getResponseWriter();
		writer.endElement("div");
		writer.write("\n"); 
	}

	@Override
	public String getFamily() {
		return "gri.tab";
	}

}