package gri;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

public class menuItem extends UIComponentBase {
	
	private String url;
	
	private String icon;
	
	private String target;
	
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Override
	public boolean getRendersChildren() {
		return true;
	}
	
	@Override
	public void encodeChildren(FacesContext context) throws IOException {
		if (this.getChildren() != null) {
			for (UIComponent uicomp : this.getChildren()) {
				if (uicomp instanceof menuItem && uicomp.isRendered()) {
					uicomp.encodeAll(context);
				}
			}
		}
	}
	
	public void encodeBegin(FacesContext context) throws java.io.IOException {
		ResponseWriter writer = context.getResponseWriter();
		writer.startElement("li", this);
		if (!(this.getParent() instanceof menuItem)) {
			writer.writeAttribute("class", "dropdown", null);
		}
		if (containChildrenMenuItem()){
			writer.writeAttribute("class", "dropdown-submenu", null);
			writer.writeAttribute("role", "menu", null);
		}
		if (childrenDoesNotContainSLink()) {
			encodeA(context, writer);
		}
		else {
			contentOfA(context, writer);
		}
		if (containChildrenMenuItem()){
			writer.startElement("ul", this);
			writer.writeAttribute("class", "dropdown-menu", null);
			writer.writeAttribute("role", "menu", null);
		}
		writer.write("\n"); 
	}

	private boolean childrenDoesNotContainSLink() {
		for (UIComponent comp : this.getChildren()) {
			if (comp.getClass().getSimpleName().equals("LinkComponent")) {
				return false;
			}
			if (comp.getClass().getSimpleName().equals("HtmlOutputLink")) {
				return false;
			}
			if (comp.getClass().getSimpleName().equals("HtmlLink")) {
				return false;
			}
			if (comp.getClass().getSimpleName().equals("UICommandLink")) {
				return false;
			}
		}
		return true;
	}

	private void encodeA(FacesContext context, ResponseWriter writer) throws IOException {
		writer.startElement("a", this);
		if (this.getTarget() != null && !this.getTarget().equals("")) {
			writer.writeAttribute("target", this.getTarget(), null);
		}
		if (containChildrenMenuItem()) {
			writer.writeAttribute("href", "#", null);
		}
		else {
			writer.writeAttribute("href", this.getUrl(), null);
		}
		if (!(this.getParent() instanceof menuItem) && this.containChildrenMenuItem()) {
			writer.writeAttribute("class", "dropdown-toggle", null);
			writer.writeAttribute("data-toggle", "dropdown", null);
			writer.writeAttribute("role", "button", null);
			writer.writeAttribute("aria-haspopup", "true", null);
			writer.writeAttribute("aria-expanded", "false", null);
		}

        contentOfA(context, writer);
		writer.endElement("a");
	}

    private void createIconSpan(ResponseWriter writer) throws IOException {
        if (this.getIcon() != null) {
            writer.startElement("span", this);
            writer.writeAttribute("class", this.getIcon(), null);
            writer.endElement("span");
        }
    }

    private void contentOfA(FacesContext context, ResponseWriter writer) throws IOException {
        createIconSpan(writer);
        if (this.getChildren() != null) {
			for (UIComponent uicomp : this.getChildren()) {
				if (!(uicomp instanceof menuItem) && uicomp.isRendered()) {
					uicomp.encodeAll(context);
				}
			}
		}
		if (!(this.getParent() instanceof menuItem) && this.containChildrenMenuItem()) {
			writer.startElement("span", this);
			writer.writeAttribute("class", "caret", null);
			writer.endElement("span");
		}
	}
	
	private boolean containChildrenMenuItem() {
		if (this.getChildren() == null) return false;
		for (UIComponent comp : this.getChildren()) {
			if (comp instanceof menuItem && comp.isRendered()) {
				return true;
			}
		}
		return false;
	}

	public void encodeEnd(FacesContext context) throws java.io.IOException {
		ResponseWriter writer = context.getResponseWriter();
		if (containChildrenMenuItem()){
			writer.endElement("ul");
		}
		writer.endElement("li");
		writer.write("\n"); 
	}

	@Override
	public String getFamily() {
		return "gri.menuItem";
	}

}